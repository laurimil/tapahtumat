export const Query = (search) => {
    return (dispatch, getState) => {
        // console.log(search)
        let searchQuery = {
            collection: 'events',
            orderBy:['timestamp', 'asc'],
            limit: 300
        }
        if(search.date){           
            const endDate = new Date(search.date.getFullYear(), search.date.getMonth(), search.date.getDate()+1)
            searchQuery.startAt = search.date.toISOString()
            console.log(search.date,endDate)
            searchQuery.endAt = endDate.toISOString()
        } else {
            searchQuery.startAt = new Date().toISOString()
        }
        if(search.city||search.category){
            searchQuery.where = []
            if(search.city){
                let array = ['city', '==', search.city]
                searchQuery.where.push(array)
            }
            if(search.category){
                let array = ['category', '==', search.category]
                searchQuery.where.push(array)
            }
        }
        if(!search.city && !search.category && search.text){
            searchQuery.where = []
            const arr = search.text.split(' ')
            if(arr.length){
                const obj = {}
                obj[arr[0]] = true
                // console.log(obj)
                let array = ['textArray', 'array-contains', obj]
                searchQuery.where.push(array)
            } else {
                const obj = {}
                let string = search.text.toLowerCase()
                obj[string] = true
                // console.log(obj)
                let array = ['textArray', 'array-contains', obj]
                searchQuery.where.push(array)
            } 
        }
        console.log(searchQuery)
        dispatch({ type: 'QUERY', query: searchQuery })
    }
}
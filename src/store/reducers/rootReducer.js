// import authReducer from './authReducer'
// import eventReducer from './eventReducer'
import searchReducer from './searchReducer'
import queryReducer from './queryReducer'
import { firestoreReducer } from 'redux-firestore'
import { combineReducers } from 'redux'

const rootReducer = combineReducers({
    // auth: authReducer,
    // events: eventReducer,
    search: searchReducer,
    query: queryReducer,
    firestore: firestoreReducer
})

export default rootReducer
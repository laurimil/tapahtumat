const initState = { 
    collection: 'events',
    orderBy:['timestamp', 'asc'],
    limit: 100,
    startAt: new Date().toISOString()
}

const queryReducer = ( state = initState, action) => {
    switch(action.type) {
        case 'QUERY':
            state = action.query
            // console.log('search changed', action.search)
            return state
        default :
            return state
    }
}

export default queryReducer
const initState = { 
    date: '',
    text: '',
    category: '',
    city: ''
}

const searchReducer = ( state = initState, action) => {
    switch(action.type) {
        case 'SEARCH':
            state = action.search
            // console.log('search changed', action.search)
            return state
        default :
            return state
    }
}

export default searchReducer
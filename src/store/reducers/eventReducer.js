const initState = {}

const eventReducer = ( state=initState, action) => {
    switch (action.type) {
        case 'GET_EVENTS':
            console.log('fetched events', action.newEvents)
            state = action.newEvents
            return state
        case 'GET_EVENTS_ERROR':
            console.log('error fetching events', action.err)
            return state
        default:
            return state
    }
}

export default eventReducer
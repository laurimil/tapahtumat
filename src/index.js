import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { reduxFirestore, getFirestore } from 'redux-firestore'
import { reactReduxFirebase, getFirebase } from 'react-redux-firebase'

import ReactGA from 'react-ga'
import 'semantic-ui-css/semantic.min.css';
import registerServiceWorker from './registerServiceWorker';

import rootReducer from './store/reducers/rootReducer'
import firebase from './config/firebase'

import App from './App'

const store = createStore(rootReducer, 
    compose(
        applyMiddleware(thunk.withExtraArgument({getFirebase, getFirestore})),
        reduxFirestore(firebase),
        reactReduxFirebase(firebase)
    )
)

ReactGA.initialize('UA-125117822-1');
ReactGA.pageview(window.location.pathname + window.location.search);

render(
    <Provider store={store}>
        <App />
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();

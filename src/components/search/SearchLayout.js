import React from 'react'
import { Dropdown, Menu } from 'semantic-ui-react'

const SearchLayout = (props) => {
    console.log(props.mobile)
    return (
        props.mobile ? 
            <Dropdown>{props.children}</Dropdown> :
            <Menu>{props.children}</Menu>
    )
}

export default SearchLayout
import React, { Component } from 'react'
import { Input, Modal, Menu, Icon, Header, Button } from 'semantic-ui-react'

import Calendar from 'react-calendar'

const locale = 'fi-FI'

class DateInput extends Component {
    constructor(props) {
        super(props)

        this.state = {
            open: false,
            value: ''
        }

        this.onChange=this.onChange.bind(this)
        this.onToggle=this.onToggle.bind(this)
    }
   
    
    onChange = date => this.setState({ date, open: false })
    onToggle = () =>  this.setState({ date: this.state.date, open: !this.state.open })
    
    render(){
        const { open, date } = this.state
        const { name, style, title, value } = this.props
        let dateString = ''
        if(value){
            dateString = value.toLocaleDateString(locale)
            console.log(dateString)
        }
       
        return (
            <Modal
                open={open} 
                dimmer='inverted'
                closeIcon
                centered={false}
                size='tiny'
                // onClick={this.onToggle}
                trigger={
                    <Menu.Item title={title} name={name} style={style}>
                        <Icon name="calendar"/>
                        <Input
                            transparent
                            fluid
                            // styles={styles.input} 
                            placeholder="Päivä"
                            value={dateString}
                            onClick={this.onToggle} 
                            />
                        {dateString ? <Icon name='close' onClick={()=>{
                            this.props.onChange(name, '')
                        }} /> : <div></div>}
                    </Menu.Item>
                }>
                <Header>
                    Valitse päivä
                    <Button 
                        content='Sulje'
                        icon='close' 
                        // labelPosition='right'
                        floated='right'
                        onClick={this.onToggle}/>
                </Header>
                <Modal.Content>
                    <Calendar 
                        onChange={(e)=>{
                            this.onChange(e)
                            this.props.onChange(name,e)
                        }}
                        value={date||null}
                        minDate={ new Date()}
                        locale={locale}
                        style={{ justifySelf: 'center' }}
                    /> 
                </Modal.Content>
                <Modal.Actions>
                    
                </Modal.Actions>
            </Modal>
        )
    }
}

// const styles = {
//     input: {
//         borderStyle: 'none'
//     }
// } 

export default DateInput
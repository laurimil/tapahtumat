import React from 'react'
import { Menu, Responsive } from 'semantic-ui-react'

import MenuIcons from './MenuIcons'
import SearchInput from './SearchInput'
import DateInput from './DateInput'
import TextInput from './TextInput'

import search from '../../common/search.json'

const SearchFields = (props) => {
    const { mobile } = props
    const { categories, cities } = search
    const { city, category, date, text } = props.search
    return (
        <Menu stackable>
            <SearchInput
                title="Hae kaupungin mukaan"
                name="city" 
                placeholder="Kaupunki"
                icon="location arrow"
                value={city}
                options={cities} 
                onChange={props.onChange}
                style={styles.search}
                fluid={mobile}
                />
            <SearchInput
                title="Hae kategorioittain"
                name="category" 
                placeholder="Kategoria"
                icon="list"
                value={category}
                options={categories} 
                onChange={props.onChange} 
                style={styles.search}
                fluid={mobile}
                />
            <DateInput
                title="Hae päivämäärällä"
                name="date"
                placeholder="Päivä"
                value={date}
                onChange={props.onChange}
                clearField={props.clearField}
                style={styles.search}
                fluid={mobile}
                />
            <TextInput
                title="Hae sanalla"
                name="text"
                placeholder="Haku"
                value={text}
                onChange={props.onChange}
                clearField={props.clearField}
                style={styles.search}
                fluid={mobile}
                />
            <Responsive as={MenuIcons} minWidth={780} getLocation={props.getLocation} clearSearch={props.clearSearch}/>
        </Menu>
    )
}

const styles = {
    menu: {
        borderStyle: 'none'
    },
    search: {
        flex: 1,
        borderStyle: 'none'
    }
}

export default SearchFields
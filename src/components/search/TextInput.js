import React from 'react'
import { Input, Menu, Icon } from 'semantic-ui-react'

const TextInput =(props) => {

    return(
        <Menu.Item style={props.style} onClick={props.handleItemClick}>
            <Icon name='search' />
            <Input
                transparent
                value={props.value}
                placeholder={props.placeholder} 
                onChange={(e,data) => props.onChange(props.name,data.value)}
                />
            { props.value 
                ? <Icon name='close' onClick={()=>props.onChange(props.name,'')} /> 
                : <div></div> }
        </Menu.Item>
    )
    
}
export default TextInput
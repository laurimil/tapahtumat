import React from 'react'
import { Menu, Responsive } from 'semantic-ui-react'

const MenuIcons = (props) => {
    
    return (
        <Menu.Menu position='right'>
            <Menu.Item
                    title='Tyhjennä haku'
                    icon={{name:'close'}} 
                    onClick={props.clearSearch}/>
            <Menu.Item 
                as={Responsive} 
                maxWidth={780} 
                icon={{name:'search'}} 
                onClick={props.onChange} />
            <Menu.Item
                title='Hae sijainnilla'
                icon={{name:'map marker'}} 
                onClick={props.getLocation}/>
        </Menu.Menu>
    )
}

export default MenuIcons
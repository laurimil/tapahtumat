import React from 'react'
import { Image, List } from 'semantic-ui-react'

import EventModal from './EventModal'

import defaultImage from '../../images/default.jpg'

const TodayEvent = (props) => {
    const { title, city, image } = props.event
    
    return (
        <EventModal event={props.event}>
            <List.Item>
                <Image avatar wrapped src={image}  onError={e => e.target.src=defaultImage}/>
                <List.Content style={{ overflow: 'hidden' }}>
                    <List.Header>{title}</List.Header>
                    <List.Description>{city}</List.Description>
                </List.Content>
            </List.Item>
        </EventModal>
    )
}

export default TodayEvent


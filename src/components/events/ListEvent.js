import React from 'react'

import { Image, Grid, Header, Label } from 'semantic-ui-react'

import EventModal from './EventModal'

import search from '../../common/search.json'
import defaultImage from '../../images/default.jpg'

const makeImages = (url) => {
    const imagesObj = {}
    const images = [
        {
            size: '60x60',
            width: '60',
            key: 'small'
        },
        {
            size: '222x222',
            width: '222',
            key: 'medium'
        },
        {
            size: '984x333',
            width: '984',
            key: 'large'
        },
    ]

    images.forEach(image => {
        let newUrl = url.replace(/60x60/g, image.size)
        newUrl = newUrl.replace(/60/g, image.width)
        imagesObj[image.key]=newUrl
    })
    return imagesObj
}

const catData = (category) => {
    const { categories } = search
    const res = categories.filter(cat => {
        if(cat.key===category) {
            return cat
        }
        return null
    })
    let color, text
    if(res[0]){
        text = res[0].text.fi
        color = res[0].color  
    } else {
        color = text = ''
    }
    
    return {text, color}
}

const days = {
    '0': 'SU',
    '1': 'MA',
    '2': 'TI',
    '3': 'KE',
    '4': 'TO',
    '5': 'PE',
    '6': 'LA',

}

const sizes = {
    desktop: {
        big: 'huge',
        medium: 'large',
        small: 'small'
    },
    mobile: {
        big: 'large',
        medium: 'medium',
        small: 'small'
    }
}

const ListEvent = (props) => {
    // console.log(props)
    const { title, venue, city, image,  timestamp, category } = props.event
    const { screen } = props
    // console.log(screen)
    const date = new Date(timestamp).getDate()
    const month =new Date(timestamp).getMonth()+1
    const day = days[new Date(timestamp).getDay()]
    const images = makeImages(image)
    const { text, color } = catData(category)
    const imageSize = 'medium'
    

    // if(event && events){
    return (
        <EventModal {...props}>
            <Grid.Row
                columns={3}
                verticalAlign='middle'
                className='event'
                // style={styles.list}
                >
                <Grid.Column width={2} textAlign='center' >
                    <Header size={sizes[screen].big} as='h3'>{day}</Header>
                    <Header size={sizes[screen].medium} as='h3'>{date}.{month}.</Header>
                </Grid.Column>
                <Grid.Column width={4}>
                    <Image size={sizes[screen].small} src={images[imageSize]} onError={e => e.target.src=defaultImage} />
                </Grid.Column>
                <Grid.Column width={10}>
                    <Header size={sizes[screen].medium} as='h5' >{title}</Header>
                    <Header size={sizes[screen].medium} as='p' >{venue}<br/>{city}</Header>
                   { category ? <Label color={color||'black'} ribbon>{ text ? text : category } </Label>:<div></div>}
                </Grid.Column>
            </Grid.Row>
        </EventModal>
    )
    // } else {
    //     return <Loader />
    // }
}

export default ListEvent


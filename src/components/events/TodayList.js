import React from 'react'

import { Loader, List, Segment, Header } from 'semantic-ui-react'

import TodayEvent from './TodayEvent'

// const listSize = 20

const searchFilter = (events, search) => {
    let filtered = events
    const {text, date, category, city } = search
    
    if(filtered&&city){
        filtered = filtered.filter(event => {
            if(event.city === city){
                return event
            }
            return null

        })
        // console.log('city filtered', filtered.length)
    }
    if(filtered&&category){
        filtered = filtered.filter(event => {
            if(event.category === category){
                return event
            }
            return null
        })
        // console.log('category filter', filtered.length)
    }
    
    if(filtered&&date){
        filtered = filtered.filter(event => {
            if(new Date(event.timestamp).toDateString() === date.toDateString()){
                // console.log('same date')
                return event
            }
            return null
        })
        // console.log('date filter', filtered.length)
    }
    if(filtered&&text){
        const words = text.split(' ')
        words.forEach(word => {
            if(word.length>1){
                filtered = filtered.filter(event => {
                    if(event.textArray&&event.textArray.filter(obj => obj[word]).length){
                        // console.log(event.eventId)
                        return event.textArray.filter(obj => obj[word])
                    }
                    return null
                })
            } 
        })
        // console.log('text filter', words, filtered.length)
    }
    return filtered
}

const getEvents = (props, search) => {
    const events = []
    Object.keys(props.events).forEach(event => events.push(props.events[event]))
    const filtered = searchFilter(events, search)
    return filtered
    
}

const renderEvents = (events) => {
    
    if(events) {
        return (
            events && events.map(event=>{
                return <TodayEvent key={event.id} event={event} />
            })
        )
    } else {
        return <Loader />
    }
   
}

const TodayList = (props) => {
    let search = { date: new Date(), text:'', category:'', city:'' }
    const today = new Date()
    const tomorrow = new Date(today.getFullYear(), today.getMonth(), today.getDate()+1)
    if(props.events){
        let events = getEvents(props, search)
        let date = today.toLocaleDateString()
        if(!events.length){
            search = {...search, date: tomorrow}
            events = getEvents(props, search)
            date = tomorrow.toLocaleDateString()
        }
        
        return (
            <div>
            <Header>Tapahtumia {date}</Header>
            <Segment style={{ overflow: 'auto', maxHeight: '70vh' }}>
                <List>
                    {renderEvents(events)}
                </List>            
            </Segment>
            </div>
        )

    } else {
        return <div></div>
    }
}

export default TodayList
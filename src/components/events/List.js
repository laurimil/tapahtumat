import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Responsive, Grid, Loader, Dimmer, Container } from 'semantic-ui-react'

import ListEvent from './ListEvent'
import PageMenu from './PageMenu'

const listSize = 20

class List extends Component {
    
    static contextTypes = {
        store: PropTypes.object.isRequired
    }
    constructor(props){
        super(props)
        this.state = {
            activePage: 1,
            pages: 0,
            list: []
        }
        this.onPageTurn = this.onPageTurn.bind(this)
        this.gotEvents = this.gotEvents.bind(this)
        this.searchFilter = this.searchFilter.bind(this)
    }
   
    
    componentDidMount() {
        this.updateQuery(this.props.query)
    }

    componentDidUpdate(prevProps, prevState) {
        // console.log('did update!')
        // console.log('component updated list',prevState.list,this.state.list)
    }

    shouldComponentUpdate(nextProps, nextState) {
        // console.log('shouldupdate?',nextState)
        // if(JSON.stringify(this.props)!==JSON.stringify(nextProps)){
        //     console.log('props changed',nextProps)
        // }
        if(JSON.stringify(this.state)!==JSON.stringify(nextState)){
            // console.log('state changed',nextState)
            return true
        }

        if(!this.props.events||Object.keys(this.props.events).length!==Object.keys(nextProps.events).length) {
            // console.log('more events')
            this.gotEvents(nextProps.events, nextProps.search)
            return true
        } 
        
        if(nextProps.search){
            if(JSON.stringify(nextProps.search)!==JSON.stringify(this.props.search)){
                console.log('search changed')
                this.updateQuery(nextProps.query)
                this.gotEvents(nextProps.events, nextProps.search)
                return true
            }
            return false
            //if search is empty
            // const update = []
            // Object.keys(nextProps.search).forEach(key => update.push(nextProps.search[key] === '' ? true : false))
            // if(!update.every((val)=>val===true)){return true}
            // console.log(JSON.stringify(nextProps.search)!==JSON.stringify(this.props.search))
            // return JSON.stringify(nextProps.search)!==JSON.stringify(this.props.search)
        }
        return true
    }

    updateQuery(query) {
        const { firestore } = this.context.store
        firestore.get(query)
    }

    gotEvents = (events, search) => {
        let allEvents = []
        let list
        if(events){
            Object.keys(events).forEach(key => allEvents.push(events[key]))
        } else {
            allEvents = this.state.list
        }
        // console.log('all events', allEvents.length, 'filter', search)
        const filtered = this.searchFilter(allEvents, search)
        list = filtered
        const pages = Math.ceil(list.length/listSize)
        // console.log('pages', pages, 'events',list.length)
        this.setState({pages, list, activePage:this.state.activePage})

        return list
    }

    getCurrent(list) {
        const current = []
        const { activePage } = this.state
        const end = activePage * listSize
        let start = end-listSize
        if(list.length){
            while(start<end){
                if(list[start]){
                    current.push(list[start])
                }
                start++
            }
        }
        return current
    }

    searchFilter(events, search) {
        let filtered = events
        const {text, date, category, city } = search
        
        if(filtered&&city){
            filtered = filtered.filter(event => {
                if(event.city === city){
                    return event
                }
                return null

            })
            // console.log('city filtered', filtered.length)
        }
        if(filtered&&category){
            filtered = filtered.filter(event => {
                if(event.category === category){
                    return event
                }
                return null
            })
            // console.log('category filter', filtered.length)
        }
        
        if(filtered&&date){
            filtered = filtered.filter(event => {
                if(new Date(event.timestamp).toDateString() === date.toDateString()){
                    // console.log('same date')
                    return event
                }
                return null
            })
            // console.log('date filter', filtered.length)
        }
        if(filtered&&text){
            const words = text.split(' ')
            words.forEach(word => {
                if(word.length>1){
                    filtered = filtered.filter(event => {
                        if(event.textArray&&event.textArray.filter(obj => obj[word]).length){
                            // console.log(event.eventId)
                            return event.textArray.filter(obj => obj[word])
                        }
                        return null
                    })
                } 
            })
            // console.log('text filter', words, filtered.length)
        }
        if(!filtered){
            const state = this.state
            state.pages = 0
            this.setState({state})
        }
        return filtered
    }

    onPageTurn = activePage => this.setState({ activePage, list: this.state.list, pages: this.state.pages })

    onSelect = id => {
        this.setState({ event: id, activePage:this.state.activePage, list: this.state.list, pages: this.state.pages})
    }
    onUpdate = e => {}

    renderEvents({screen}) {
        // console.log('render')
        if(this.props.events) {
            const { list } = this.state
            const current = this.getCurrent(list)
            if(list.length){
                const rendered = current.length ? current : list
                return (
                    rendered && rendered.map(event=>{
                        return (
                            <ListEvent 
                                key={event.id} 
                                event={event} 
                                {...this.props} 
                                screen={screen}
                                onClick={this.onSelect}/>
                        )
                    })
                )
            } else {
                return <div>Haulle ei löytynyt tuloksia</div>
            }
        } else {
            return  <Dimmer active inverted>
                        <Loader inverted>Lataa</Loader>
                    </Dimmer>
        }
        
    }

    render() {
        const { activePage, pages } = this.state
        return (
            <div>
                { pages
                    ?   <PageMenu 
                            pages={ pages} 
                            onChange={(e, data) =>this.onPageTurn(data.activePage)} 
                            activePage={activePage} /> 
                    :   <div></div> }
                <Container>
                    {/* Fullscreen List*/}
                    <Grid as={Responsive} 
                        minWidth={780}
                        onUpdate={this.onUpdate}
                        {...Responsive.onlyLargeScreen}
                        verticalAlign='middle' 
                        >
                        {this.renderEvents({screen:'desktop'})}
                    </Grid>
                    {/* Mobile List*/}
                    <Grid as={Responsive} 
                        maxWidth={780} 
                        onUpdate={this.onUpdate}
                        {...Responsive.onlyMobile}
                        divided='vertically'>
                        {this.renderEvents({screen:'mobile'})}
                    </Grid>
                </Container>
                { pages
                    ? <PageMenu 
                        pages={pages} 
                        onChange={(e, data) =>this.onPageTurn(data.activePage)} 
                        activePage={activePage} /> 
                    : <div></div>}
            </div>                          
        )
    }
}

const mapStateToProps = (state) => {
    // console.log(state)
    return {
        events: state.firestore.data.events,
        query: state.query,
        search: state.search
    }
}

export default connect(mapStateToProps)(List)